### simulation/optimization

All torque control with bezier curves

### hardware control

Get Bezier curve path of the leg from simulation/optimization and  have an Impedance control (flight stage) and torque control (stance phase)

---
## Optimization 

### Variables

Two Bezier curves for torques:
$$
ctrl_1=[T_1,T_2,..T_n] \ \ \ \ \ \  \\
ctrl_2=[T_1,T_2,..T_n] \\
\theta_1 \\ 
\theta_2 \\ 
\dot{\theta}_1 \\ 
\dot{\theta}_2 \\  
\dot{x} \\  
\dot{y} \\ 
\ \\
-0.85<T_i<0.85 \ \ \ n=10  \\
-60 \degree<\theta_1<-25\degree \\
110 \degree<\theta_2<140\degree  \\
-0.1<\dot{\theta}_1 <0.1\\
-0.1<\dot{\theta}_2<0.1\\
-0.1<\dot{x}<0.1\\
-0.1<\dot{y}<0.1\\
$$

Starting Conditions:

$$
ground \ height = -0.164; \\
\theta_1=-40\degree \\
\theta_2=120\degree\\
x=y=0\\
\dot{\theta}_1=\dot{\theta}_2= 0\\
\dot{x}=\dot{y}= 0\\
$$

### Constraints

$$
x^{end}>=x^{start} \\
q_1^{end}=q_1^{start} \\
q_2^{end}=q_2^{start} \\
y^{end}=y^{start} \\

\dot{q}_1^{end}=\dot{q}_1^{start} \\
\dot{q}_2^{end}=\dot{q}_2^{start} \\
\dot{x}^{end}=\dot{x}^{start} \\
\dot{y}^{end}=\dot{y}^{start} \\
$$

### Objective Function

1. First objective: maximize height $`h'`$ to push it to go up, got the max $`x'`$
2. Then I added to the constraints:
    $$
    apex \ height = h' \\
    x_{end}=x' \\
    $$ 
    and minimized cost of transport by minimizing:
    $$
    E/(m*g*d)
    $$ 
    which is the similar as minimizing sum of torque squared as m, g and d is fixed
