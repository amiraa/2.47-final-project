function [t_out, z_out, u_out,s_out] =simulate_leg_trajectory(z0,p,ground,trajectory1,trajectory2,trajectory3)  
        
    %% to remove
    p_traj.omega = 30;
    p_traj.omega = 3;
    p_traj.x_0   = 0;
    p_traj.y_0   = -.125-0.05;
    p_traj.r     = 0.025*1.5;
    
    tf=trajectory1.tf+trajectory2.tf+trajectory3.tf;
    
    
    %% Perform Dynamic simulation
    dt = 0.005;
    
    num_step = floor(tf/dt);
    tspan = linspace(0, tf, num_step); 
    t_out=tspan;
    
    u_out = zeros(2,num_step);
    z_out = zeros(8,num_step);
    s_out = zeros(num_step,1);
    z_out(:,1) = z0;
    
    z_des=z0(1:2);
    
    for i=1:num_step-1
        
        rE = position_foot(z_out(:,i), p);
        if rE(2) > ground.ground_height
            s_out(i)=1;
            if(s_out(i-1)==0)
                z_des=z_out(1:2,i-1);
            end
        end
        

        [dz,u] = dynamics(tspan(i), z_out(:,i), p, trajectory1,trajectory2,trajectory3);
        
        % Velocity update with dynamics
        z_out(:,i+1) = z_out(:,i) + dz*dt;
        
        z_out(5:8,i+1) = joint_limit_constraint1(z_out(:,i+1),p);
        z_out(5:8,i+1) = discrete_impact_contact(z_out(:,i+1),p, ground);
%         
        % Position update
        z_out(1:4,i+1) = z_out(1:4,i) + z_out(5:8,i+1)*dt;
        
%         z_out(3:4,i+1)=[0;0];
%         z_out(7:8,i+1)=[0;0];
        
        u_out(:,i+1)=u;
    end
    
 
          
    
end


function [dz,tau] = dynamics(t,z,p,trajectory1,trajectory2,trajectory3)
    % Get mass matrix
    A = A_leg(z,p);
    
    % Compute Controls
    %tau = control_law_torque(t,z,p,ctrl1,ctrl2,state,z_des);
    tau = control_law(t,z,p,trajectory1,trajectory2,trajectory3);
    % tau = control_law_Impedance(t,z,p,trajectory1,trajectory2,trajectory3);

    
    % Get b = Q - V(q,qd) - G(q)
    b = b_leg(z,tau,p);
    
    % Solve for qdd.
    qdd = A\(b);
    dz = 0*z;
    
    % Form dz
    dz(1:4) = z(5:8);
    dz(5:8) = qdd;
end

%Torque Control Law
function tau = control_law_torque(t, z, p,ctrl1,ctrl2,state,z_des)
    %tau1 = BezierCurve(ctrl1.T, t/ctrl1.tf);
    %tau2 = BezierCurve(ctrl2.T, t/ctrl2.tf);
    if(state==0)
        tau1 = interpolate(ctrl1.T, t/ctrl1.tf);
        tau2 = interpolate(ctrl2.T, t/ctrl2.tf);
    else
        th1 = z(1,:);            % leg angle
        th2 = z(2,:);            % leg angle
        
        dth1 = z(5,:);           % leg angular velocity
        dth2 = z(6,:);           % leg angular velocity

        thd1 = z_des(1);             % desired leg angle
        thd2 = z_des(2);             % desired leg angle
        
        k = 5;                  % stiffness (N/rad)
        b = .5;                 % damping (N/(rad/s))
        
        k = 1;                  % stiffness (N/rad)
        b = .1;                 % damping (N/(rad/s))

        tau1 = -k*(th1-thd1) - b*dth1;% apply PD control
        tau2 = -k*(th2-thd2) - b*dth2;% apply PD control
    end
        
    
    tau =[tau1;tau2];
end
    
%Impedance Control Law
function tau = control_law_Impedance(t, z, p, trajectory1,trajectory2,trajectory3)
    % Controller gains, Update as necessary for Problem 1
    K_x = 150.; % Spring stiffness X
    K_y = 150.; % Spring stiffness Y
    D_x = 10.;  % Damping X
    D_y = 10.;  % Damping Y
    
%     K_x = 10.; % Spring stiffness X
%     K_y = 10.; % Spring stiffness Y
%     D_x = 5.;  % Damping X
%     D_y = 5.;  % Damping Y


    
    % Desired position of foot is a circle
    if t<=trajectory1.tf
        rEd(1) = -BezierCurve(trajectory1.pts(1,:), t/trajectory1.tf);
        rEd(2) = BezierCurve(trajectory1.pts(2,:), t/trajectory1.tf);

    elseif t<=trajectory1.tf+trajectory2.tf
        rEd(1) = -BezierCurve(trajectory2.pts(1,:), (t-trajectory1.tf)/trajectory2.tf);
        rEd(2) = BezierCurve(trajectory2.pts(2,:), (t-trajectory1.tf)/trajectory2.tf);

    else
        rEd(1) = -BezierCurve(trajectory3.pts(1,:), (t-trajectory1.tf-trajectory2.tf)/trajectory3.tf);
        rEd(2) = BezierCurve(trajectory3.pts(2,:), (t-trajectory1.tf-trajectory2.tf)/trajectory3.tf);

    end
    
          
    rEd=[rEd 0]';
    
    % Compute desired velocity of foot
    vEd = [1 1 0]';
    % Desired acceleration
    aEd = [0 0 0]';
    
    % Actual position and velocity 
    rE = position_foot(z,p);
    vE = velocity_foot(z,p);
    
    % Jacobian matrix \partial r_E / \partial q
    J  = jacobian_foot(z,p);
    dJ = jacobian_dot_foot(z,p);
    J=J(1:2,1:2);
    dJ=dJ(1:2,1:2);
    
    dq = z(5:6);

    % Compute virtual foce for Question 1.4 and 1.5
    f  = [K_x * (rEd(1) - rE(1) ) + D_x * (vEd(1) - vE(1) ) ;
          K_y * (rEd(2) - rE(2) ) + D_y * (vEd(2) - vE(2) ) ];
    
    %% Task-space compensation and feed forward for Question 1.8
    % Get joint space components of equations of motion
    Mass_Joint_Sp = A_leg(z,p);
    Grav_Joint_Sp = Grav_leg(z,p);
    Corr_Joint_Sp = Corr_leg(z,p);

    Mass_Joint_Sp=Mass_Joint_Sp(1:2,1:2);
    Grav_Joint_Sp=Grav_Joint_Sp(1:2);
    Corr_Joint_Sp=Corr_Joint_Sp(1:2);
    
    Mass_Joint_Sp_inv = inv(Mass_Joint_Sp);
    % Task-space mass matrix (Equaiton 51 in Khatib's paper)
    Lambda = inv(J * Mass_Joint_Sp_inv * J');
    
    % Coriolis force in task-space (Equation 51)
    mu     = Lambda*J*Mass_Joint_Sp_inv* Corr_Joint_Sp - Lambda * dJ * dq;
    
    % Gravity force in task-space (Equation 51)
    rho    = Lambda*J*Mass_Joint_Sp_inv * Grav_Joint_Sp; 
    
    % Add task-space acceleration force feedforward, coriolis, and gravity compensation 
    f(1:2) = Lambda*(aEd(1:2) + f(1:2)) + mu + rho; % OSC
%     f(1:2) = Lambda*(aEd(1:2) + f(1:2)) + rho; % OSC w/o mu (coriolis)
%     f(1:2) = Lambda*(aEd(1:2) + f(1:2)) + mu; % OSC w/o rho (gravity)
    
    % Map to joint torques  
    tau = J' * f;
end

function tau = control_law(t,z,p,trajectory1,trajectory2,trajectory3)
    % Controller gains, Update as necessary for Problem 1
    K_x = 40; % Spring stiffness X
    K_y = 40; % Spring stiffness Y
    D_x = 4;  % Damping X
    D_y = 4;  % Damping Y
    
    K_x = 40; % Spring stiffness X
    K_y = 40; % Spring stiffness Y
    D_x = 4;  % Damping X
    D_y = 4;  % Damping Y
    
%     K_x = 0; % Spring stiffness X
%     K_y = 0; % Spring stiffness Y
%     D_x = 0;  % Damping X
%     D_y = 0;  % Damping Y
    
        % Desired position of foot is a circle
    if t<=trajectory1.tf
        rEd(1) = -BezierCurve(trajectory1.pts(1,:), t/trajectory1.tf);
        rEd(2) = BezierCurve(trajectory1.pts(2,:), t/trajectory1.tf);

    elseif t<=trajectory1.tf+trajectory2.tf
        rEd(1) = -BezierCurve(trajectory2.pts(1,:), (t-trajectory1.tf)/trajectory2.tf);
        rEd(2) = BezierCurve(trajectory2.pts(2,:), (t-trajectory1.tf)/trajectory2.tf);

    else
        rEd(1) = -BezierCurve(trajectory3.pts(1,:), (t-trajectory1.tf-trajectory2.tf)/trajectory3.tf);
        rEd(2) = BezierCurve(trajectory3.pts(2,:), (t-trajectory1.tf-trajectory2.tf)/trajectory3.tf);

    end
    
    rEd=[rEd 0]';
    
    % Compute desired velocity of foot
    vEd = [1 1 0]';
    
%     % Desired position of foot is a circle
%     rEd = [p_traj.x_0 p_traj.y_0 0]' + ...
%             p_traj.r*[cos(p_traj.omega*t) sin(p_traj.omega*t) 0]';
%     % Compute desired velocity of foot
%     vEd = p_traj.r*[-sin(p_traj.omega*t)*p_traj.omega    ...
%                      cos(p_traj.omega*t)*p_traj.omega   0]';

    % Actual position and velocity 
    rE = position_foot(z,p);
    vE = velocity_foot(z,p);
    
    % Jacobian matrix \partial r_E / \partial q
    J  = jacobian_foot(z,p);
    J=J(1:2,1:2);
    
    % Compute virtual foce for Question 1.4 and 1.5
    f  = [K_x * (rEd(1) - rE(1) ) - D_x * (vE(1) - vEd(1) ) ;
          K_y * (rEd(2) - rE(2) ) - D_y * (vE(2) - vEd(2) ) ;];
    
    % Map to joint torques  
    tau = J' * f;
end

%% Discrete Contact
function qdot = discrete_impact_contact(z,p,ground)
    rest_coeff=ground.restitution_coeff;
    fric_coeff=ground.friction_coeff;
    yC=ground.ground_height;
    
    qdot = z(5:8);
    rE = position_foot(z, p);
    vE = velocity_foot(z, p);

    if(rE(2)-yC<0 && vE(2) < 0)
      J  = jacobian_foot(z,p);
      A = A_leg(z,p);
      Ainv = inv(A);
      
      J_z = J(2,:);
      lambda_z = 1/(J_z * Ainv * J_z.');
      F_z = lambda_z*(-rest_coeff*vE(2) - J_z*qdot);
      qdot = qdot + Ainv*J_z.'*F_z;
      
      % horizontal
      J_x = J(1,:);
      lambda_x = 1/(J_x * Ainv * J_x.');
      F_x = lambda_x * (0 - J_x * qdot);
      if( abs(F_x) > fric_coeff*F_z)
          F_x = sign(F_x)*F_z*fric_coeff;
      end
      qdot = qdot + Ainv*J_x.'*F_x;
    z_test = z;
    z_test(5:8) = qdot;
    vE = velocity_foot(z_test, p);
    end

end

function qdot = joint_limit_constraint1(z,p)
    

    q2_min = (90-50) * pi/ 180;
    q2_max = (90+50) * pi/ 180;
    q1_min = (-90) * pi/ 180;
    q1_max = (90) * pi/ 180;
    qdot = z(5:8);
    
    
    
    %%%% %% q2_min
    
    C = z(2) - q2_min; % C gives distance away from constraint
    dC= z(6);
    
    J = [0 1 0 0];
    A = A_leg(z,p);
%     A=A(1:2,1:2);
    
    if (C < 0 && dC <0)% if constraint is violated
        lambda = A(2,2);
        F_c = lambda * (0 - dC);
        qdot = qdot + inv(A)*J.'*F_c;        
    end
    
    
    %% q2_max
    C = q2_max-z(2) ; % C gives distance away from constraint
    dC= z(6);
  
    if (C < 0 && dC >0)% if constraint is violated
        lambda = A(2,2);
        F_c = lambda * (0 - dC);
        qdot = qdot + inv(A)*J.'*F_c;        
    end
    
    %% q1_min
    C = z(1) - q1_min; % C gives distance away from constraint
    dC= z(5);
    
    J = [1 0 0 0];
    A = A_leg(z,p);
%     A=A(1:2,1:2);
    
    if (C < 0 && dC <0)% if constraint is violated
        lambda = A(1,1);
        F_c = lambda * (0 - dC);
        qdot = qdot + inv(A)*J.'*F_c;        
    end
    
    
    %% q1_max
    C = q1_max-z(1) ; % C gives distance away from constraint
    dC= z(5);
  
    if (C < 0 && dC >0)% if constraint is violated
        lambda = A(1,1);
        F_c = lambda * (0 - dC);
        qdot = qdot + inv(A)*J.'*F_c;        
    end
    
%     qdot =qdot(1:2);
 
end

%% joint Constraint
function z1 = joint_limit_constraint(z,p)
     q1_min = (-90) * pi/ 180;
    q1_max = (90) * pi/ 180;
    
    q2_min=(90-50)*pi/180;
    q2_max=(90+55)*pi/180;
    
    z1=z;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if z(1)<q1_min || z(2)>q1_max
        z1(5)=0;
    end
    if z(1)<q1_min
        z1(1)=q1_min;
    end
    if z(1)>q1_max
        z1(1)=q1_max;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if z(2)<q2_min || z(2)>q2_max
        z1(6)=0;
    end
    if z(2)<q2_min
        z1(2)=q2_min;
    end
    if z(2)>q2_max
        z1(2)=q2_max;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
end




