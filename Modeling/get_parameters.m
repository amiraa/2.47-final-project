function p=get_parameters()
    %% Definte fixed paramters
    m1 =.0393 + .2;         m2 =.0368; 
    m3 = .00783;            m4 = .0155;
    
    I1 = 25.1 * 10^-6;      I2 = 53.5 * 10^-6;
    I3 = 9.25 * 10^-6;      I4 = 22.176 * 10^-6;
%     127mm
    l_OA=.011;              l_OB=.042; 
    l_AC=.096;              l_DE=.091;
    l_O_m1=0.032;           l_B_m2=0.0344; 
    l_A_m3=0.0622;          l_C_m4=0.0610;
    
    %% Definte fixed paramters
    m0=0.214;
%     m0=0;
    m1 =.2607;         m2 =.036; 
    m3 = .009;         m4 = .027;
    
    l_OA=0.03673;           l_OB=.06773;
    l_AB =l_OB-l_OA;
    l_AC =.096;              l_DE=0.127-l_AB;
    l_O_m1=0.051;           l_B_m2=0.0345; 
    l_A_m3=0.0600;          l_C_m4=0.065;
    
    
    th01= -177.27*pi/180;
    th02= 180*pi/180;
    kappa1=0.00;
    kappa2=0.00;
    
    
    
    N = 18.75;
    Ir = 0.0035/N^2;
    g = 9.81/2;    
     %% Parameter vector
    % p   = [m1 m2 m3 m4 I1 I2 I3 I4 Ir N l_O_m1 l_B_m2 l_A_m3 l_C_m4 l_OA l_OB l_AC l_DE g]';
    p   = [th01 th02 kappa1 kappa2 m0 m1 m2 m3 m4 I1 I2 I3 I4 Ir N l_O_m1 l_B_m2 l_A_m3 l_C_m4 l_OA l_OB l_AC l_DE g]';        % parameters

end