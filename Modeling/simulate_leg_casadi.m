function [t_out, z_out, u_out] =simulate_leg_casadi(z0,p,ground,ctrl1,ctrl2,ctrl3)  

%     pf=position_foot(z0,p);
%     ground.ground_height=pf(2);

    num_step=ctrl1.n+ctrl2.n+ctrl3.n;
    
    % Declare casadi symbolic variables
    t_out = casadi.MX(zeros(1,num_step));
    z_out = casadi.MX(zeros(8,num_step));
    u_out = casadi.MX(zeros(2,num_step-1));
    
    
    % Time discretization
    
    t_out(1) = 0;
    dt1 = ctrl1.tf/(ctrl1.n-1);
    dt2 = ctrl1.tf/(ctrl2.n-1);
    dt3 = ctrl1.tf/(ctrl3.n-1);
    i=2;
    for j = 1:ctrl1.n-1
        t_out(i) = t_out(i-1) + dt1;
        i=i+1;
    end
    for j = 1:ctrl2.n
        t_out(i) = t_out(i-1) + dt2;
        i=i+1;
    end
    for j = 1:ctrl3.n
        t_out(i) = t_out(i-1) + dt3;
        i=i+1;
    end
    
    
%     ctrl2.q1T=[0];
%     ctrl2.q2T=[0];
%     
    
    
    %% Perform Dynamic simulation
    z_out(:,1) = z0;
    
    for j=1:ctrl1.n
        
        i=j;
        t = t_out(i);
        [dz,u] = dynamics(t, z_out(:,i), p, ctrl1);
        
        % Velocity update with dynamics
        z_out(:,i+1) = z_out(:,i) + dz*dt1;
        
        z_out(5:8,i+1) = discrete_impact_contact_stance(z_out(:,i+1),p, ground);
        
        % Position update
        z_out(1:4,i+1) = z_out(1:4,i) + z_out(5:8,i+1)*dt1 ;
        
        u_out(:,i)=u;
    end
    for j=1:ctrl2.n
        
        i=j+ctrl1.n;
        
        t = t_out(i);
        [dz,u] = dynamics(t, z_out(:,i), p, ctrl2);
        
        % Velocity update with dynamics
        z_out(:,i+1) = z_out(:,i) + dz*dt2;
        
        % Position update
        z_out(1:4,i+1) = z_out(1:4,i) + z_out(5:8,i+1)*dt2 ;
        
        u_out(:,i)=u;
    end
    
    for j=1:ctrl3.n-1
        
        i=j+ctrl2.n+ctrl3.n;
        t = t_out(i);
        [dz,u] = dynamics(t, z_out(:,i), p, ctrl3);
        
        % Velocity update with dynamics
        z_out(:,i+1) = z_out(:,i) + dz*dt3;
        
        z_out(5:8,i+1) = discrete_impact_contact_stance(z_out(:,i+1),p, ground);

        % Position update
        z_out(1:4,i+1) = z_out(1:4,i) + z_out(5:8,i+1)*dt3 ;
        
        u_out(:,i)=u;
    end
    

 

  
end

%Torque Control Law
function tau = control_law_torque(t, z, p,ctrl)
    tau1 = BezierCurve(ctrl.q1T, t/ctrl.tf);
    tau2 = BezierCurve(ctrl.q2T, t/ctrl.tf);

%     tau1 = interpolate(ctrl.q1T, t/ctrl.tf);
%     tau2 = interpolate(ctrl.q2T, t/ctrl.tf);

    tau =[tau1;tau2];
    
    
%     th = z(2,:);            % leg angle
%     dth = z(4,:);           % leg angular velocity
% 
%     thd = pi/4;             % desired leg angle
%     k = 5;                  % stiffness (N/rad)
%     b = .5;                 % damping (N/(rad/s))
% 
%     u = -k*(th-thd) - b*dth;% apply PD control
    
end

function [dz,tau] = dynamics(t,z,p,ctrl)
    % Get mass matrix
    A = A_leg(z,p);
    
    % Compute Controls
    tau = control_law_torque(t,z,p,ctrl);
        
    % Get b = Q - V(q,qd) - G(q)
    b = b_leg(z,tau,p);
    
    % Solve for qdd.
    qdd = A\(b);
    %     dz = 0*z;
    
    dz = casadi.MX(zeros(8,1));
    % Form dz
    dz(1:4,1) = z(5:8);
    dz(5:8,1) = qdd(1:4,1);
end

%% Discrete Contact Stance
function qdot = discrete_impact_contact_stance(z,p,ground)
    rest_coeff=ground.restitution_coeff;
    fric_coeff=ground.friction_coeff;
    yC=ground.ground_height;
    
    qdot = z(5:8);
    
    rE = position_foot(z, p);
    vE = velocity_foot(z, p);

    %if(rE(2)-yC<0 && vE(2) < 0)
      
        J  = jacobian_foot(z,p);
        A = A_leg(z,p);
        Ainv = inv(A);

        J_z = J(2,:);
        lambda_z = 1/(J_z * Ainv * J_z.');
        F_z = lambda_z*(-rest_coeff*vE(2) - J_z*qdot);
        qdot = qdot + Ainv * J_z.'*F_z;

        % horizontal
%         J_x = J(1,:);
%         lambda_x = 1/(J_x * Ainv * J_x.');
%         F_x = lambda_x * (0 - J_x * qdot);
%         if( abs(F_x) > fric_coeff*F_z)
%           F_x = sign(F_x)*F_z*fric_coeff;
%         end
%         qdot = qdot + Ainv*J_x.'*F_x;
%         z_test = z;
%         z_test(5:8) = qdot;
%         vE = velocity_foot(z_test, p);
    %end

end




