function u=interpolate(ctrl_pt,t)

    n = length(ctrl_pt)-1;
    x = 0:(1/n):1;
    u = interp1(x,ctrl_pt,t);
    
    
    
    
end