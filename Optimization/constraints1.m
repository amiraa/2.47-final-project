function [cineq ceq] = constraints1(x,z_target,z0,p,ground)
% Inputs:
% x - an array of decision variables.
% z0 - the initial state
% p - simulation parameters
% 
% Outputs:
% cineq - an array of values of nonlinear inequality constraint functions.  
%         The constraints are satisfied when these values are less than zero.
% ceq   - an array of values of nonlinear equality constraint functions.
%         The constraints are satisfied when these values are equal to zero.
%
% Note: fmincon() requires a handle to an constraint function that accepts 
% exactly one input, the decision variables 'x', and returns exactly two 
% outputs, the values of the inequality constraint functions 'cineq' and
% the values of the equality constraint functions 'ceq'. It is convenient 
% in this case to write an objective function which also accepts z0 and p 
% (because they will be needed to evaluate the objective function).  
% However, fmincon() will only pass in x; z0 and p will have to be
% provided using an anonymous function, just as we use anonymous
% functions with ode45().
    tf=x(1);
    numCtrlPoints=(length(x)-1)/2;
 
    ctrl1.tf = tf;                                  % control time points
    ctrl1.T = x(2:(1+numCtrlPoints));

    ctrl2.tf = tf;                                  % control time points
    ctrl2.T = x((1+numCtrlPoints+1):end);
    
    [t, z, u,s_out] = simulate_leg(z0,p,ground,tf,ctrl1,ctrl2); % run simulation
    
%     [minTheta,minThetat] = min(z(2,:));
%     [maxTheta,maxThetat] = max(z(2,:));
%     
%     
%     cineq = [-minTheta,maxTheta-3.14159/2.0];
%     
%     cineq1 = -min(z(2,:));
%     cineq2 = max(z(2,:))-3.14159/2;
%     cineq = [cineq1, cineq2];   
%     ceq = [x(2) - t(indices(1))]; % prob 5 
%     
%     
%     COM = COM_jumping_leg(z,p);
%     
%     [maxy,maxyI] = max(COM(2,:));
%     vel=max(COM(4,maxyI));
%     
%     ceq = [x(2) - t(indices(1)), maxy-0.4,vel];   % prob 6

    
    
    q1_start=z_target(1) ;
    q2_start=z_target(2) ;
    x_start= z_target(3);
    y_start=z_target(4);
    dq1_start=z_target(5) ;
    dq2_start=z_target(6) ;
    dx_start= z_target(7);
    dy_start=z_target(8);
    
    q1_end=z(1,end) ;
    q2_end=z(2,end) ;
    x_end= z(3,end);
    y_end=z(4,end);
    dq1_end=z(5,end) ;
    dq2_end=z(6,end) ;
    dx_end= z(7,end);
    dy_end=z(8,end);
    
%     minAngle2=min(z(2,:) );
%     maxAngle2=max( z(2,:) );
    
%     cineq=[x_start-x_end, ...
%         maxAngle2- (90+55),(50-90)-minAngle2];
    
    cineq=[x_start-x_end];
    cineq=[];
    ceq=[q1_start-q1_end,q2_start-q2_end, ...
        y_start-y_end, ...
        dq1_start-dq1_end,dq2_start-dq2_end, ...
        dx_start-dx_end,dy_start-dy_end];
%     ceq=[q1_start-q1_end,q2_start-q2_end, ...
%         y_start-y_end];
                                                            
% simply comment out any alternate constraints when not in use
    
end