function f = objective1(x)
% Inputs:
% x - an array of decision variables.
% z0 - the initial state
% p - simulation parameters
% 
% Outputs:
% f - scalar value of the function (to be minimized) evaluated for the
%     provided values of the decision variables.
%
% Note: fmincon() requires a handle to an objective function that accepts 
% exactly one input, the decision variables 'x', and returns exactly one 
% output, the objective function value 'f'.  It is convenient for this 
% assignment to write an objective function which also accepts z0 and p 
% (because they will be needed to evaluate the objective function).  
% However, fmincon() will only pass in x; z0 and p will have to be
% provided using an anonymous function, just as we use anonymous
% functions with ode45().
    tf=x(1);
%     numCtrlPoints=(length(x)-1)/2;
%  
%     ctrl1.tf = tf;                                  % control time points
%     ctrl1.T = x(2:(1+numCtrlPoints));
% 
%     ctrl2.tf = tf;                                  % control time points
%     ctrl2.T = x((1+numCtrlPoints+1):end);
%     
%     [t, z, u] = simulate_leg(z0,p,ground,tf,ctrl1,ctrl2); % run simulation
    

    f=tf;
    

end