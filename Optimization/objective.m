function f = objective(x,z0,p,ground,tf)
% Inputs:
% x - an array of decision variables.
% z0 - the initial state
% p - simulation parameters
% 
% Outputs:
% f - scalar value of the function (to be minimized) evaluated for the
%     provided values of the decision variables.
%
% Note: fmincon() requires a handle to an objective function that accepts 
% exactly one input, the decision variables 'x', and returns exactly one 
% output, the objective function value 'f'.  It is convenient for this 
% assignment to write an objective function which also accepts z0 and p 
% (because they will be needed to evaluate the objective function).  
% However, fmincon() will only pass in x; z0 and p will have to be
% provided using an anonymous function, just as we use anonymous
% functions with ode45().
    tf=x(end);
    numCtrlPoints=(length(x)-7)/2;
    
    z0=[x(1);x(2);z0(3);z0(4);x(3);x(4);x(5);x(6)];

    pf=position_foot(z0,p);
    ground.ground_height=pf(2);

    ctrl1.tf = tf;                                  % control time points
    ctrl1.T = x(7:(6+numCtrlPoints));

    ctrl2.tf = tf;                                  % control time points
    ctrl2.T = x((6+numCtrlPoints+1):end-1);
    
    [t, z, u,s_out] = simulate_leg(z0,p,ground,tf,ctrl1,ctrl2); % run simulation
    

    x_end= z(3,end);
    
    f = -x_end;                     % negative of stride
    
%     m=1;
%     d=x_end;
%     g=9.8;
%     uu=u*u';
%     CoT= E/(m*g*d)

    x_end= z(3,end);

    f = -max(z(4,:));                     % negative of height
    
    
    PF=position_foot(z,p)-ground.ground_height;
    f = -( max(PF(2,:)) );
    
    f = abs( max(PF(2,:))-0.1 );
    
    
    % alternate objective functions:
%     f = tf;                                         % final time
    
%     ctrl_t = linspace(0, ctrl.tf, 50);
%     ctrl_pt_t = linspace(0, ctrl.tf, length(ctrl.T));
%     n = length(ctrl_t);
%     ctrl_input = zeros(1,n);
% 
%     for i=1:n
%         ctrl_input(i) = BezierCurve(x(3:end),ctrl_t(i)/ctrl.tf);
%     end
%     
    
%     f = sum(sum(ctrl_input));                                      % minimize T^2 integral
%     size(ctrl_input);
%     cc=ctrl_input*ctrl_input'/50;
%     size(u);


%     uu=u*u';
%     f = uu; 
end