function animate_solution(t_out, z_out, u_out, p, ground, ctrl1, ctrl2, saveVideo)
    
%     clf;
%     plot_control(ctrl1,u_out(1,:),1)
%     clf;
%     plot_control(ctrl2,u_out(2,:),4)
    
    %% Animate Solution
    figure(30); clf;
    hold on

    plot([-2 2],[ground.ground_height ground.ground_height],'k'); 
    animateSol(t_out, z_out,p,saveVideo);
    
   


end

function animateSol(tspan, x,p,saveVideo)
    % Prepare plot handles
    hold on
    
    if saveVideo
        %Initialize video
        myVideo = VideoWriter('myVideoFile'); %open video file
        myVideo.FrameRate = 10;  %can adjust this, 5 - 10 works well for me
        open(myVideo);
    end
    
    
    h_OB = plot([0],[0],'LineWidth',2);
    h_AC = plot([0],[0],'LineWidth',2);
    h_BD = plot([0],[0],'LineWidth',2);
    h_CE = plot([0],[0],'LineWidth',2);
    h_trajec = plot([0],[0],'LineWidth',1);
    
    lastX=[];
    lastY=[];
    count=1;
    
    xlabel('x'); ylabel('y');
    h_title = title('t=0.0s');
    
    axis equal

    %Step through and update animation
    for i = 1:length(tspan)
        % skip frame.
        if mod(i,10) && i~=1
            continue;
        end
        t = tspan(i);
        z = x(:,i); 
        keypoints = keypoints_leg(z,p);
        
        r0 = keypoints(:,6);
        rA = keypoints(:,1); % Vector to base of cart
        rB = keypoints(:,2);
        rC = keypoints(:,3); % Vector to tip of pendulum
        rD = keypoints(:,4);
        rE = keypoints(:,5);
        
        
        
        
        lastX(count)=rE(1);
        lastY(count)=rE(2);
        
        set(h_trajec,'XData',lastX);
        set(h_trajec,'YData',lastY);
        
        
        
        minimumX=min(keypoints(1,:))-0.2;
        maximumX=max(keypoints(1,:))+0.2;
        axis([minimumX maximumX -.3 .1]);

        set(h_title,'String',  sprintf('t=%.2f',t) ); % update title
        
        set(h_OB,'XData',[r0(1) rB(1)]);
        set(h_OB,'YData',[r0(2) rB(2)]);
        
        set(h_AC,'XData',[rA(1) rC(1)]);
        set(h_AC,'YData',[rA(2) rC(2)]);
        
        set(h_BD,'XData',[rB(1) rD(1)]);
        set(h_BD,'YData',[rB(2) rD(2)]);
        
        set(h_CE,'XData',[rC(1) rE(1)]);
        set(h_CE,'YData',[rC(2) rE(2)]);
        if saveVideo
            if mod(i,800)
                frame = getframe(gcf);
                writeVideo(myVideo, frame);
            end
        end
        count=count+1;
        pause(.01)
    end
    if saveVideo
        close(myVideo); 
    end
end
