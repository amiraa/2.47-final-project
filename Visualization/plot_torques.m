function plot_torques(t_out,theta_0,s_out,thetas,u,ctrl,figNum)
    prob='6.';
    uu=u*u';
    stance=0.3;
    flight=0.57;
    tf=ctrl.tf;
    
    clf
    plot_control(ctrl,u,figNum+7);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(figNum+6)  % stance and flight phases
    clf
    hold on
    
    
%     patch([0 stance stance 0], [max(ylim) max(ylim) 0 0], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)
%     patch([stance flight flight stance], [max(ylim) max(ylim) 0 0], [242, 198, 194]/245,'DisplayName',['flight'])
%     patch([flight tf tf flight], [max(ylim) max(ylim) 0 0], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)

%     patch([0 0.3 0.3 0], [max(ylim) max(ylim) 0 0], 'b')
%     patch([0.3 0.57 0.57 0.3], [max(ylim) max(ylim) 0 0], 'g')
%     patch([0.57 1 1 0.57], [max(ylim) max(ylim) 0 0], 'b')


    scatter(t_out(:),s_out(:),'DisplayName',['phases']);
    
    
    hold off
    xlabel('time (s)')
    ylabel('Phase (stance=0, flight=1)')
    title(strcat('Phases',''))

    legend('Location','bestoutside')
    filename=strcat('./img/',prob,num2str(figNum),'_phases.png');
    fig=gcf;
    fig.Position(3:4)=[300,200];
    saveas(fig,filename);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    
    num_step=100;
    num_stiffness=10;
    vals=zeros(100,num_stiffness);
    
    
    thetass=linspace(theta_0*180/pi-90, theta_0*180/pi+90, num_step);
    
    maxK=0.25;
    ks=linspace(maxK/num_stiffness, maxK, num_stiffness);
    for j=1:num_stiffness
        for i=1:num_step
%             deltaThetas=angdiff(thetas,ones(1,length(thetas))*thetass(i)*pi/180);
            u1=u- (thetas-(thetass(i)*pi/180))*ks(j);
%             u1=u- (deltaThetas)*ks(j);
            vals(i,j)=u1*u1';
        end
    end
    
    
    
    figure(figNum+2)  % control input profile
    clf
    hold on
    
   
    plot(thetass, ones(1,num_step)*uu(1,1),'Color',[1.0,0.0,0.0],'DisplayName',['k=' num2str(0)]);
    
    
    for j=1:num_stiffness
         plot (thetass, vals(:,j),'Color',[0,0.7*j/num_stiffness,0.9*j/num_stiffness],'DisplayName',['k=' num2str(ks(j))]);
    end
    
    
    
    
    hold off
    xlabel('Neutral Angles')
    ylabel('Total Sum of Torques Squared')
    title(strcat('Total Sum of Torques Squared',''))
    legend('Location','bestoutside')
    fig=gcf;
    fig.Position(3:4)=[300,200];
    
    filename=strcat('./img/',prob,num2str(figNum),'_ks.png');
    saveas(fig,filename);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    
    minMatrix = min(vals(:));
    [row,col] = find(vals==minMatrix)
    display("neutral Angle:"+thetass(row));
    display("Stiffness:"+ks(col));
    theta_0=thetass(row)*pi/180;
    k=ks(col);
    
    num_step=100;
    
    thetass=linspace(thetass(row)-30, thetass(row)+30, num_step);
    vals=zeros(30,num_stiffness);

    for j=1:num_stiffness
        for i=1:num_step
            u1=u- (thetas-(thetass(i)*pi/180))*ks(j);
            vals(i,j)=u1*u1';
        end
    end
    
    
    
    figure(figNum+3)  % control input profile
    clf
    hold on
    
    plot(thetass, ones(1,num_step)*uu(1,1),'Color',[1.0,0.0,0.0],'DisplayName',['k=' num2str(0)]);

    for j=1:num_stiffness
         plot (thetass, vals(:,j),'Color',[0,0.7*j/num_stiffness,0.9*j/num_stiffness],'DisplayName',['k=' num2str(ks(j))]);
    end
    
    hold off
    xlabel('Neutral Angles')
    ylabel('Total Sum of Torques Squared')
    title(strcat('Total Sum of Torques Squared Zoomed',''))
    legend('Location','bestoutside')
    fig=gcf;
    fig.Position(3:4)=[300,200];
        
    filename=strcat('./img/',prob,num2str(figNum),'_ks_zoom.png');
    saveas(fig,filename);
    
    
    
   
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%     minMatrix = min(vals(:));
%     [row,col] = find(vals==minMatrix)
%     display("neutral Angle:"+thetass(row));
%     display("Stiffness:"+ks(col));
    
    
    figure(figNum)  % control input profile
    clf
    hold on
    
%     deltaThetas=angdiff(thetas,ones(1,length(thetas))*theta_0);
	deltaThetas=thetas-theta_0;

    u1 = u- deltaThetas*k;
    b=sum(u1>u)/length(thetas)*100;
    
    uu1=u1*u1';
    
    
    minn=min([u u1]);
    maxx=max([u u1]);
    
%     patch([0 stance stance 0], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)
%     patch([stance flight flight stance], [maxx maxx minn minn], [242, 198, 194]/245,'DisplayName',['flight'])
%     patch([flight tf tf flight], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)

%     plot(t_out, u.^2,'Color',[1,0.0,0.0],'DisplayName',['without spring=' num2str(uu)]);
%     plot(t_out, u1.^2,'Color',[0,0.7,0.9],'DisplayName',['with spring=' num2str(uu1)]);
    plot(t_out, u,'Color',[1,0.0,0.0],'DisplayName',['without spring=' num2str(uu)]);
    plot(t_out, u1,'Color',[0,0.7,0.9],'DisplayName',['with spring=' num2str(uu1)]);
    
    
    hold off
    xlabel('time (s)')
    ylabel('torque (Nm)')
    title(strcat('Torques',''))
    legend('Location','bestoutside')
    fig=gcf;
    fig.Position(3:4)=[400,200];
        
    filename=strcat('./img/',prob,num2str(figNum),'_torques_with_without.png');
    saveas(fig,filename);
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     deltaThetas=angdiff(thetas,ones(1,length(thetas))*theta_0);

    figure(figNum+5)  % control input profile
    clf
    hold on
    
    minn=min([u(:).*u(:);u1(:).*u1(:)]);
    maxx=max([u(:).*u(:);u1(:).*u1(:)]);
%     
%     patch([0 stance stance 0], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)
%     patch([stance flight flight stance], [maxx maxx minn minn], [242, 198, 194]/245,'DisplayName',['flight'])
%     patch([flight tf tf flight], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)

    
%     plot(t_out, u.^2,'Color',[1,0.0,0.0],'DisplayName',['without spring=' num2str(uu)]);
%     plot(t_out, u1.^2,'Color',[0,0.7,0.9],'DisplayName',['with spring=' num2str(uu1)]);
    plot(t_out, u(:).*u(:),'Color',[1,0.0,0.0],'DisplayName',['without spring=' num2str(uu)]);
    plot(t_out, u1(:).*u1(:),'Color',[0,0.7,0.9],'DisplayName',['with spring=' num2str(uu1)]);
    
    
    hold off
    xlabel('time (s)')
    ylabel('torque^2 (Nm*Nm)')
    title(strcat('Torques Squared',''))
    legend('Location','bestoutside')
    fig=gcf;
    fig.Position(3:4)=[400,200];
        
    filename=strcat('./img/',prob,num2str(figNum),'_torques_with_without_squared.png');
    saveas(fig,filename);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(figNum+4)  % control input profile
    clf
    hold on
    
    minn=min(thetas-theta_0);
    maxx=max(thetas-theta_0);
    
%     patch([0 stance stance 0], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)
%     patch([stance flight flight stance], [maxx maxx minn minn], [242, 198, 194]/245,'DisplayName',['flight'])
%     patch([flight tf tf flight], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)

    
    
    plot(t_out, (thetas-theta_0),'Color',[0,0.7,0.9],'DisplayName',['thetas']);
    
    hold off
    xlabel('time (s)')
    ylabel('Angles (Degrees)')
    title(strcat('Angles Difference',''))
    legend('Location','bestoutside')
    fig=gcf;
    fig.Position(3:4)=[300,200];
    
    filename=strcat('./img/',prob,num2str(figNum),'_diffangles.png');
    saveas(fig,filename);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(figNum+1)  % control input profile
    clf
    hold on
    
    minn=min([thetas thetas*180/pi]);
    maxx=max([thetas thetas*180/pi]);
    
%     patch([0 stance stance 0], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)
%     patch([stance flight flight stance], [maxx maxx minn minn], [242, 198, 194]/245,'DisplayName',['flight'])
%     patch([flight tf tf flight], [maxx maxx minn minn], [194, 224, 242]/245,'DisplayName',['stance'],'Transparency',0.1)

    
    plot(t_out, thetas*180/pi,'Color',[0,0.7,0.9],'DisplayName',['thetas']);
    plot(t_out, ones(size(thetas))*theta_0*180/pi,'Color',[1,0.0,0.0],'DisplayName',['neutral angle']);
    
    hold off
    xlabel('time (s)')
    ylabel('Angles (Degrees)')
    title(strcat('Theta vs Neutral Angle',''))
    legend('Location','bestoutside')
    fig=gcf;
    fig.Position(3:4)=[300,200];
    
    filename=strcat('./img/',prob,num2str(figNum),'_torques_thetast.png');
    saveas(fig,filename);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end

