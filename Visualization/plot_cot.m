function plot_cot(p,tf,t_out,z_out,u_out,figNum)
    R=2; K_t=0.188;
    
    prob='6.';
    
    m=p(5)+p(6)+p(7)+p(8)+p(9);
    g=p(end);
    d=z_out(3,end);
    
    
    tau1=u_out(1,:);
    tau2=u_out(2,:);
    cot_no_spring=cost_of_transport(tau1,tau2,m,d,tf,g,R,K_t);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    theta_0=0;
    thetas=z_out(1,2:end);
    
    
    num_step=100;
    num_stiffness=30;
    vals=zeros(100,num_stiffness);
    
    
    thetass=linspace(theta_0*180/pi-90, theta_0*180/pi+90, num_step);
    
    maxK=0.1;
    ks=linspace(maxK/num_stiffness, maxK, num_stiffness);
    for j=1:num_stiffness
        for i=1:num_step
            tau11=tau1-(thetas-(thetass(i)*pi/180))*ks(j);
            vals(i,j)=cost_of_transport(tau11,tau2,m,d,tf,g,R,K_t);
        end
    end
    
    
    
    figure(figNum+1)  % control input profile
    clf
    hold on
%     patch([0 0.25 0.25 0], [max(ylim) max(ylim) 0 0], 'r')
%     patch([0.25 0.5 0.5 0.25], [max(ylim) max(ylim) 0 0], 'g')
%     patch([0.5 1 1 0.5], [max(ylim) max(ylim) 0 0], 'b')

    plot(thetass, ones(1,num_step)*cot_no_spring,'Color',[1.0,0.0,0.0],'DisplayName',['k=' num2str(0)]);

    for j=1:num_stiffness
         plot (thetass, vals(:,j),'Color',[0,0.7*j/num_stiffness,0.9*j/num_stiffness],'DisplayName',['k=' num2str(ks(j))]);
    end
    
    
    
    
    hold off
    xlabel('Neutral Angles')
    ylabel('Cost of Transport')
    legend('Location','bestoutside');
    
    
    filename=strcat('./img/',prob,num2str(figNum),'_cot1.png');
    fig=gcf;
    fig.Position(3:4)=[300,200];
    saveas(fig,filename);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    theta_0=90*pi/180;
    thetas=z_out(2,2:end);
    
    
    num_step=100;
    num_stiffness=100;
    vals=zeros(100,num_stiffness);
    
    
    thetass=linspace(theta_0*180/pi-90, theta_0*180/pi+90, num_step);
    
    maxK=0.1;
    ks=linspace(maxK/num_stiffness, maxK, num_stiffness);
    for j=1:num_stiffness
        for i=1:num_step
            tau22=tau2-(thetas-(thetass(i)*pi/180))*ks(j);
            vals(i,j)=cost_of_transport(tau1,tau22,m,d,tf,g,R,K_t);
        end
    end
    
    
    
    figure(figNum+2)  % control input profile
    clf
    hold on
    
    
    
%     plot(thetass, ones(1,num_step)*cot_no_spring,'Color',[1.0,0.0,0.0],'DisplayName',['k=' num2str(0)]);
    
    
%     for j=1:num_stiffness
%          plot (thetass, vals(:,j),'Color',[0,0.7*j/num_stiffness,0.9*j/num_stiffness],'DisplayName',['k=' num2str(ks(j))]);
%     end
%     
%     
%     
%     
%     hold off
%     xlabel('Neutral Angles')
%     ylabel('Cost of Transport')
%     legend('Location','bestoutside');
%     fig=gcf;
%     fig.Position(3:4)=[300,200];
    
    filename=strcat('./img/',prob,num2str(figNum),'_cot2.png');
    saveas(fig,filename);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(figNum+3)  % control input profile
    clf
    hold on
    j=40;
    while j<=100
        plot (ks, vals(j,:),'Color',[0,0.7*j/100,0.9*j/100],'DisplayName',['theta_0=' num2str(j/100*180)]);
        j=j+10;
        j
    end
% 	plot(ks, vals(end,:),'Color',[1.0,0.0,0.0],'DisplayName',['k=']);

    
    hold off
    xlabel('Spring Stiffness')
    ylabel('Cost of Transport')
    legend('Location','bestoutside');
    fig=gcf;
    fig.Position(3:4)=[300,200];
    
    filename=strcat('./img/',prob,num2str(figNum),'_stifness.png');
    saveas(fig,filename);
    
    
end

function cot=cost_of_transport(tau1,tau2,m,d,tf,g,R,K_t)
    I1=tau1/K_t;
    I2=tau2/K_t;
    dt=tf/length(tau1);
    
    E1=(I1*R*dt)*I1';
    E2=(I2*R*dt)*I2';
    
    E=E1+E2;
   
    % I=tau/K_t;
    % P=I*I*R;
    v=d/tf;
%     cot=P/(m*v*g);
    cot=E/(m*d*g);
end

