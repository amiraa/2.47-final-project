function plot_control(ctrl,u,figNum)
    prob='6.';
    id=2;
    
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(figNum);clf;  % control input profile
    ctrl_t = linspace(0, ctrl.tf, length(u));
    ctrl_pt_t = linspace(0, ctrl.tf, length(ctrl.T));
    n = length(ctrl_t);
    ctrl_input = zeros(1,n);
    ctrl_input_interpolate = zeros(1,n);
    
    for i=1:n
        ctrl_input(i) = BezierCurve(ctrl.T,ctrl_t(i)/ctrl.tf);
        ctrl_input_interpolate(i) = interpolate(ctrl.T,ctrl_t(i)/ctrl.tf);

    %     ctrl_input(i) = BezierCurve(x(3:end),ctrl_t(i)/ctrl.tf);
    end

    hold on

    plot(ctrl_t, u,'DisplayName',['Real Tau']);
%     plot(ctrl_t, ctrl_input,'DisplayName',['Desired']);
    plot(ctrl_t, ctrl_input_interpolate,'DisplayName',['Desired']);
    plot(ctrl_pt_t, ctrl.T, 'o','DisplayName','Control Points');
    % plot(ctrl_pt_t, x(3:end), 'o');
    hold off
    xlabel('time (s)')
    ylabel('torque (Nm)')
    title('Control Input Trajectory')
    legend('Location','bestoutside');
    fig=gcf;
    fig.Position(3:4)=[300,200];

    filename=strcat('./img/',prob,num2str(figNum),'_cntrl.png');    
    saveas(fig,filename);
end